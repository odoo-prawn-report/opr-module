{
    'name' : 'Ruby Report',
    'version' : '0.1',
    'author' : 'Mithril Informatique',
    'sequence': 120,
    'category': '',
    'website' : 'https://www.mithril.re',
    'summary' : '',
    'description' : "",
    'depends' : [
        'base',
        'sale',
    ],
    'data' : [
    ],

    'installable' : True,
    'application' : False,
}
