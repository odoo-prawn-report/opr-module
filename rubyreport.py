# -*- coding: utf-8 -*-
##############################################################################
#
#    rubyreport module for Odoo/OpenERP
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv
import urllib
import os


class Report(osv.Model):
  _name = "report"
  _inherit = "report"
  
  def get_prawn_report(self, ids, report_name):
    
    param = '-'.join(map(str, ids))
    url = "http://127.0.0.1:4567"
    dossier = "tmp"
    
    if not os.path.exists(dossier):
      os.makedirs(dossier)
      
    fichier = dossier+"/"+report_name+"-"+param+".pdf"
    urllib.urlretrieve (url+"/"+report_name+"/"+param, fichier)
    content = ''
    
    with open(fichier) as pdfdocument:
      content = pdfdocument.read()
    
    os.remove(fichier)       
    
    return content
  
  def get_pdf(self, cr, uid, ids, report_name, html=None, data=None, context=None):
    
    if (report_name == 'sale.report_saleorder' 
        or report_name == 'account.report_invoice'
        or report_name == 'stock.report_picking' 
        or report_name == 'sav.intervention'
        or report_name == 'purchase.report_purchaseorder'
        or report_name == 'purchase.report_purchasequotation'):
      return self.get_prawn_report(ids, report_name)
    else:
      return super(Report, self).get_pdf(cr, uid, ids, report_name, html=html, data=data, context=context)
